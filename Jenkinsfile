pipeline{
    agent any
    parameters{
        string(name: "HOST_USER", defaultValue: "ubuntu")
        string(name: "HOST_IP", defaultValue: "3.131.91.186")
        string(name: "REGISTRY_URL", defaultValue: "registry-itau.mastertech.com.br")
        string(name: "TARGET", defaultValue: "/home/ubuntu")
        string(name: "MYIMAGE", defaultValue: "${env.REGISTRY_URL}/marcele-image")
    }

    stages{
        stage("Test"){
            steps{
                script{
                    def port = sh (
                        script: "awk -v min=64000 -v max=65000 'BEGIN{srand(); print int(min+rand()*(max-min+1))}'",
                        returnStdout: true
                    ).trim()
                    /*
                    Utilizamos esse bloco withEnv para definir variáveis de ambiente que serão visíveis no escopo
                    desse stage 'Test'
                    */
                    withEnv([
                    "mysql_host=localhost",
                    "mysql_port=${port}",
                    "mysql_user=root",
                    "mysql_pass=123"]) {
                        docker.image("mysql:latest").withRun("-e 'MYSQL_ROOT_PASSWORD=123' -p ${port}:3306") { c ->
                            sh "while ! mysqladmin ping -h0.0.0.0 -P ${port} --silent; do sleep 1; done"
                            sh "./mvnw test"
                        }
                    }
                }
            }
        }
        stage("Build"){
            when{branch "master"}
            /*
            Todos os blocos de stage (Package, Build and push image e Deploy serão executados apenas
            quando o evento disparado pelo SCM for na branch master.
            */
            stages{
                stage("Package") {
                    when{branch "master"}
                    steps {
                        script{
                            sh "./mvnw package -DskipTests"
                        }
                    }
                }
                stage("Build and push image"){
                    steps{
                        script {
                            /*
                            Novamente estamos definindo as variáveis de ambiente correspondentes às credenciais do bando de dados
                            de 'produção' pois usaremos essas para contruir a imagem do Docker.
                            */
                            withEnv([
                            "mysql_host=rds-spec-3.c8tdp5ff2x6f.us-east-2.rds.amazonaws.com",
                            "mysql_port=3306",
                            "mysql_user=root",
                            "mysql_pass=mastertech"]) {
                                /*
                                Essas variáveis contendo as credenciais do banco precisar estar disponíveis quando o container for
                                iniciado e não somente durante a etapa de build da imagem, por isso estamos passando essas variáveis
                                como argumento para o comando build que será substituída dentro do Dockerfile.
                                PS a explicação disso continua no arquivo Dockerfile.
                                */
                                docker.withRegistry("https://${env.REGISTRY_URL}","registry_credential"){
                                    def customImage=docker.build("${env.MYIMAGE}", "--build-arg mysql_host=${mysql_host} --build-arg mysql_port=${mysql_port} --build-arg mysql_user=${mysql_user} --build-arg mysql_pass=${mysql_pass} .")
                                    /* Os dois 'push' abaixo fazem o push da imagem para o registry com a tag latest e a tag do numero do build da pipeline */
                                    customImage.push("${env.BUILD_ID}")
                                    customImage.push("latest")
                                }
                            }
                        }
                    }
                }
                stage("Deploy"){
                    steps{
                        sh "ssh -o StrictHostKeyChecking=no -t ${env.HOST_USER}@${env.HOST_IP} 'docker pull ${env.MYIMAGE}:latest'"
                        sh "ssh -o StrictHostKeyChecking=no -t ${env.HOST_USER}@${env.HOST_IP} 'docker stop marcele-image'"
                        sh "ssh -o StrictHostKeyChecking=no -t ${env.HOST_USER}@${env.HOST_IP} 'docker run --rm -p 8000:8080 -d --name marcele-image ${env.MYIMAGE}:latest'"
                    }
                }
            }
        }
    }
}
